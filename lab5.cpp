/*
    Philip Merry
    Username: Pmerry
    Class: CPSC 1021
    Section: 
*/


#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
    Suit suit;
    int value;
} Card;

string get_suit_code(Card& c);


string get_card_name(Card& c);

bool suit_order(const Card& lhs, const Card& rhs);

int myrandom (int i) 
{ 
    return(std::rand()%i);
}
int main(int argc, char const *argv[]) {
    // IMPLEMENT as instructed below
    /*This is to seed the random generator */
    srand(unsigned (time(0)));

    /*Create a deck of cards of size 52 (hint this should be an array) and
    *initialize the deck*/
    //Create deck
    Card* deck;
    deck = (Card*) malloc(52 * sizeof(Card)); 
    //Check existence of deck pointer
    if(!deck)
    {
	cout << endl << "Failed to allocate memory for a deck";
	exit(1);
    }
    // Initialize deck sequentially
    for(int i=0; i <= 13; i++) //initialize spades, same for other suits
    {
	deck[i].suit = SPADES; //spades = 0 
	deck[i].value = i;
    }
    for(int i=14; i <= 26; i++)
    {
	deck[i].suit = HEARTS; // hearts = 1
	deck[i].value = i-14;
    }
    for(int i=27; i <= 39; i++)
    {
	deck[i].suit = DIAMONDS; // diamonds = 3
	deck[i].value = i-27;
    }
    for(int i=40; i <= 52; i++)
    {
	deck[i].suit = CLUBS; // clubs = 4
	deck[i].value = i-40;
    }
    /*After the deck is created and initialzed we call random_shuffle() see the
    *notes to determine the parameters to pass in.*/
    random_shuffle(deck, deck+52, myrandom); // randomly shuffles from first to last card

    /*Build a hand of 5 cards from the first five cards of the deck created
	*above*/
    Card* hand; // make an array of cards to be the hand
    hand = (Card*) malloc( 5 * sizeof (Card));
    for(int j = 0; j < 5; j++)//"draw" the hand one at a time
    {
	hand[j] = deck[j];
    }
        /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/

    sort(hand, hand+5, suit_order); // call library function to do sort for me

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
     cout << setw(10) << right; // redundant but not removing cause code works
    for(int o = 0; o < 5; o++)
    {
	cout << setw(10) << right  <<endl; //set print formatting
    	cout << get_card_name(hand[o]) ;
	//cout << "wow3";
	cout << " of " << get_suit_code(hand[o]);
	cout << endl;
    }
    return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) 
{
    if( lhs.suit < rhs.suit )
    {
	return(true);
    }else if(lhs.suit == rhs.suit){
	if(lhs.value < rhs.value)
	{
	    return(true);
	}else{
	    return(false);
	}
    }else{
	return(false);
    }
}

string get_suit_code(Card& c) {
    switch (c.suit) {
	case SPADES:    return "\u2660";
	case HEARTS:    return "\u2661";
	case DIAMONDS:  return "\u2662";
	case CLUBS:     return "\u2663";
	default:        return "";
  }
}

string get_card_name(Card& c) 
{
	if(c.value <  9)
	{
	    int val = c.value + 2; //add to to make vals match up
	    //string value = itoa(val, value, 10);
	    return to_string(val);
	}else if(c.value == 9){
	    return "Jack";
	}else if(c.value == 10){
	    return  "Queen";
	}else if(c.value == 11){
	    return "King";
	}else if(c.value == 12){
	    return "Ace";
	}else
	    return "No_Card_Value";
}
